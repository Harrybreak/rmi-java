package chat;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IAccueil extends Remote{
	
	IChatroom connect(IParticipant p,String s) throws RemoteException;
	String[] listClassroom()throws RemoteException;

}
