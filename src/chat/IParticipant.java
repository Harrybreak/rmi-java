package chat;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IParticipant extends Remote {

	/**
	 * When the server receive a message from a participant
	 * 
	 * @param msg
	 * @throws RemoteException
	 */
	public void receive(String name, String msg) throws RemoteException;
	
	/**
	 * Getter for the participant name
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public String name() throws RemoteException;
}
