package chat;

public class TooManyRequests extends Exception {

	/**
	 * Serial Version UID by default (1)
	 */
	private static final long serialVersionUID = 1L;
	
	public TooManyRequests() {}
	
	public TooManyRequests(String s) {	super(s);	}

}
