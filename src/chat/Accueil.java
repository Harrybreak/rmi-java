package chat;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Iterator;

public class Accueil extends UnicastRemoteObject implements IAccueil{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<IChatroom> list;

	protected Accueil() throws RemoteException {
		super();
		list=new ArrayList<IChatroom>();
		// TODO Auto-generated constructor stub
	}

	@Override
	public IChatroom connect(IParticipant p, String s) throws RemoteException {
		/**
		 * Look for existing chatroom
		 */
		Iterator<IChatroom> iter = list.iterator();
		while (iter.hasNext()) {
			IChatroom chat = iter.next();
			if (chat.name().equals(s))
				// Returned as a reference
				return chat;
		}
		/*
		 * Create new Chatroom if none exists
		 */
		IChatroom newchat = new Chatroom(s);
		list.add(newchat);
		// Returned as a reference
		return newchat;
		
	}


	@Override
	public String[] listClassroom() throws RemoteException {
		String[] stringlist = new String[list.size()];
		Iterator<IChatroom> iter = list.iterator();
		int i=0;
		while (iter.hasNext()) {
			IChatroom chat = iter.next();
			stringlist[i] = chat.name();
			i++;
		}
		return stringlist;
	}
}
