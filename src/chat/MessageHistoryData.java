package chat;

import java.io.Serializable;

public class MessageHistoryData implements Serializable, IMessageHistoryData {

	/**
	 * The default uuid you know it
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Buffer size
	 */
	private static final int  bufferSize = 128;
	
	/**
	 * Index to put next message
	 */
	private int ind_put;
	
	/**
	 * String array of last sent messages
	 * 
	 * This array is a loop buffer associated with an input indexation
	 * which represents the last message put's offset in it minus one.
	 */
	private String[] buffer = new String[bufferSize];
	
	public MessageHistoryData() {
		this.ind_put = 0;
	}

	public void putMessage(String p, String msg) {
		this.buffer[this.ind_put++] = "[" + p + "]: " + msg;
		if (this.ind_put >= bufferSize)
			this.ind_put = 0;
	}
	
	@Override
	public String[] getLasts(int n) throws TooManyRequests {
		if (n > bufferSize)
			throw new TooManyRequests("You have requested a number of message higher than the buffer size of chatrooms !");
		if (n < 0)
			throw new TooManyRequests("You have requested a negative number of message to back");
		
		
		// Array length
		int l = n;
		// Start index
		int index = (this.ind_put - n + bufferSize) % bufferSize;
		// Eventual null messages
		while (this.buffer[index] == null && l > 0) {
			l--;
			index = (index + 1) % bufferSize;
		}
		// The returned array
		String[] returnd = new String[l];
		// Main loop
		for (int i = 0 ; i < l ; i++) {
			returnd[i] = this.buffer[index];
			index = (index + 1) % bufferSize;
		}
		
		return returnd;
	}
}
