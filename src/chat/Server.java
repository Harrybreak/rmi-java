package chat;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {
	public static void main(String[] args) throws RemoteException, AlreadyBoundException {
		try {
			Registry reg = LocateRegistry.createRegistry(9999);
			IAccueil accueil= (IAccueil) new Accueil();
			System.out.println("Server running ...");
			reg.bind("accueil", accueil);	
		} catch (Exception e) {
			System.out.println("Server closed, see you later !");
		}
	}

}
