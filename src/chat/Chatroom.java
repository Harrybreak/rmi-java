package chat;

import java.rmi.ConnectException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class Chatroom extends UnicastRemoteObject implements IChatroom {

	/**
	 * the super uuid you know it brrr
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * List of connected participants
	 */
	ArrayList<IParticipant> participant;
	
	/**
	 * Chatroom name
	 */
	String name;
	
	/**
	 * Message history manager
	 */
	MessageHistoryData mhd;
	
	protected Chatroom(String name) throws RemoteException {
		super();
		participant=new ArrayList<IParticipant>();
		this.name=name;
		this.mhd = new MessageHistoryData();
	}

	@Override
	public String name() throws RemoteException {
		return name;
	}

	@Override
	public IMessageHistoryData connect(IParticipant p) throws RemoteException {
		participant.add(p);
		return mhd;
	}

	@Override
	public void leave(IParticipant p) throws RemoteException {
		participant.remove(p);
	}

	@Override
	public String[] who() throws RemoteException {
		ArrayList<IParticipant> delete = new ArrayList<IParticipant>();
		IParticipant temp = null;
		String[] list = new String[participant.size()];
		
		
		for (int i = 0 ; i < participant.size() ; i++) {
			try {
				temp = participant.get(i);
				list[i] = temp.name();
			} catch (ConnectException e) {
				delete.add(temp);
				list[i] += " (got disconnected)";
			}
		}
		
		for (int i = 0 ; i < delete.size() ; i++) {
			this.leave(delete.get(i));
		}
		
		return list;
	}

	@Override
	public void send(IParticipant p, String msg) throws RemoteException {
		// Check if there isn't interrupted members
		ArrayList<IParticipant> delete=new ArrayList<IParticipant>();
		IParticipant temp = null;
		for (int i = 0 ; i < participant.size() ; i++) {
			try {
				temp = participant.get(i);
				// Put it in message history manager
				mhd.putMessage(p.name(), msg);
				// Then send it to everyone
				temp.receive(p.name(),msg);
			} catch (ConnectException e) {
				delete.add(temp);
			}
			
		}
		
		for (int i = 0 ; i < delete.size() ; i++) {
			participant.remove(delete.get(i));
		}
	}
	

}
