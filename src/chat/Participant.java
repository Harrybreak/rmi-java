package chat;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Participant extends UnicastRemoteObject implements IParticipant {

	/**
	 * Defaut serial UID
	 */
	private static final long serialVersionUID = 1L;
	
	private String name;

	protected Participant(String name) throws RemoteException {
		super();
		
		this.name = name;
	}

	@Override
	public void receive(String name, String msg) throws RemoteException {
		System.out.println("["+name+"]: "+msg);
	}

	@Override
	public String name() throws RemoteException {
		return this.name;
	}

}
