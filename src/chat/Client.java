package chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import java.sql.Timestamp;

public class Client {

	/**
	 * On buffer message history by default
	 */
	public static int  mhdBuffer = 10;
	
	/**
	 * Chatroom name by default
	 */
	public static String crName = "main";

	/**
	 * Port
	 */
	public static int port = 9999;

	/**
	 * Address
	 */
	public static String adr = "localhost";

	/**
	 * Participant name default
	 */
	public static String name = "Patrick";

	/**
	 * Reference to the history message data
	 */
	public static IMessageHistoryData mhd;

	/**
	 * main function
	 * @param args
	 * @throws InterruptedException
	 * @throws IOException
	 */

	public static void main(String[] args) throws InterruptedException, IOException {
		boolean available = true;
		boolean trytoreconnect = false;
		Timestamp timestampinit=null;
		Timestamp timestamp=null;

		// First of all, parse arguments
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			if (arg.equals("-a"))
				adr = args[++i];
			else if (arg.equals("-p"))
				port = Integer.parseInt(args[++i]);
			else if (arg.equals("-n"))
				name = args[++i];
			else if (arg.equals("-c"))
				crName = args[++i];
			else if (arg.equals("-h"))
				mhdBuffer = Integer.parseInt(args[++i]);
		}

		// Create participant
		Participant pc = new Participant(name);
		
		while (available) {
			// Surronding every task in this try catch clause
			try {
				// Get server lobby and chatroom from this lobby (IAccueil)
				Registry reg = LocateRegistry.getRegistry(adr, port);

				IAccueil ac = (IAccueil) reg.lookup("accueil");

				IChatroom cr = ac.connect(pc, crName);

				trytoreconnect = false;
				// Connect to this chatroom
				// This will never fail because there is still no limit capacity
				// Update name chatroom serveur
				crName = cr.name();
				mhd = cr.connect(pc);

				System.out.println("Welcome in " + crName + " chat !");
				
				// Print message history
				try {
					for (String i : mhd.getLasts(mhdBuffer)) {
						System.out.println(i);
					}
				} catch (TooManyRequests e) {
					System.out.println("/!\\ UNABLE TO LOAD CHATROOM HISTORY /!\\");
				}

				// Enter the magic world of corentin chatroom
				cr.send(new Participant("CONSOLE"), "<<< "+name+" joined in! >>>");
				String msg = "";
				while (true) {
					// Getting msg from user
					BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
					msg = bufferRead.readLine();

					// Msg binding
					if (msg.equals("Who?")) {
						System.out.println("Participants : ");
						for (String i : cr.who()) {
							System.out.println(" - " + i);
						}
					} else if (msg.equals("Bye!")) {
						// End of chating
						cr.leave(pc);
						System.out.println("Chat closed, bye!");
						return;
					}
					// Msg sending
					else
						cr.send(pc, msg);
				}

			} catch (RemoteException e) {
				// reconnection to server
					if (!trytoreconnect) {
						System.out.println("A Remote Exception occured !");
						System.out.println("Trying to reconnect . . .");
						//init timer
						timestampinit = new Timestamp(System.currentTimeMillis());
						trytoreconnect = true;
					}
					timestamp = new Timestamp(System.currentTimeMillis());
					if (timestamp.getTime() - timestampinit.getTime() > 10000) {
						System.out.println("Connection failed! Server must be down, please try later...");
						available=false;
						return;
					}
			} catch (NotBoundException e) {
				System.out.println("A Not Bound Exception occured !");
				System.out.println("Please try later...");
			}
		}
	}
}
