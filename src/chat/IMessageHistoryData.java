package chat;

import java.rmi.Remote;

public interface IMessageHistoryData extends Remote {
	/**
	 * Back the last 10 messages in a String array
	 * @return
	 */
	String[] getLasts(int n) throws TooManyRequests;
}
