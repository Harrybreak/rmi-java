package babystep;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {
	public static void main(String[] args) throws RemoteException, AlreadyBoundException {
		Registry reg = LocateRegistry.createRegistry(9999);
		IPrinter printer= (IPrinter) new Printer();
		System.out.println("server running");
		reg.bind("LinePrinter", printer);
		// La JVM server s'arrête au bout d'un certain timeout quand il n'y a plus
		// d'objets instanciés localement ou à distance.
		System.out.println("Server running ...");
	}
}
