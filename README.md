# RMI-Java AppRep Project
> Thomazo Corentin, Puech Lilian

## Introduction

Welcome into our chatroom application!
This application does still not support distant connections.
The folder ``p3_3`` contains the .jar files.

## Usage

### Server side

If you want to host a chatroom on your computer, launch ``Server.jar`` program.
No option available.

### Client side

If you want to connect to a chatroom server, launch ``Client.jar`` program.
There are multiple arguments available:

* **-c** : expect 1 String argument. Specifies the name chatroom to connect in. If the chatroom's name does not exist, the server will create it for you. Default chatroom's name is "main".
* **-n** : expect 1 String argument. Specifies your nickname. Default nickname is "Patrick".
* **-p** : expect 1 Integer argument. Specifies the local port to use. Default port is 9999.
* **-h** : expect 1 Integer argument. Specifies the size of the message history to load when joining a chatroom. Default number of messages to load is 10.
* **-a** : expect 1 IP Address argument. Specifies the IP address of the server machine to connect to. Default address is "localhost".

### Chating

There are multiple secret messages:

* **"Who?"** : display off the list of connected members to the chatroom. It can also display the recent disconnected members.
* **"Bye!"** : disconnect from the chatroom, but the client keep alive. To shutdown it, just force it to close with the red cross or [Ctrl]+[C].

## Exceptions

In case of trouble while connecting to the server, you may raise some exceptions like ``A Remote Exception occurred! Please Try Later!``.

In case of occurred errors, the client tries to connect to the server for 10 more seconds before cancelling requests, but it keeps alive. To shutdown it, just force it to close using the red cross or using [Ctrl]+[C].

